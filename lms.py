from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time, random
import traceback

url= 'https://www.cambridgelms.org/main/p/splash'
username = "admin" # INSERT YOUR LOGIN
password = "admin" # INSERT YOUR PASS
unit = 7 # CHOOSE UNIT 
section = 1 # Section to start: A-1, B-2...
# Delay for "doing" a task, should be at least 5 sec to load page (random, check str 41)
start = 5 
stop = 12

settings = Options()
settings.add_extension('package/extension.crx')
driver = webdriver.Chrome('package/chromedriver.exe', chrome_options = settings)
driver.get(url)
time.sleep(3)
driver.switch_to_frame(driver.find_element_by_id("cas_iframe"))
driver.find_element(By.XPATH,'/html/body/div/div/form/fieldset/ol/li[1]/input').send_keys(username)
driver.find_element(By.XPATH,'/html/body/div/div/form/fieldset/ol/li[2]/input').send_keys(password)
driver.find_element(By.XPATH,'/html/body/div/div/form/fieldset/ol/li[3]/button').click()
time.sleep(3)
driver.find_element(By.XPATH,'/html/body/div[3]/div/div/section/div/div/div/div/div/div/div/section/section/section/ul/li/div/a/span').click()
time.sleep(5)
driver.find_element(By.XPATH,'/html/body/div[2]/div/div/section/div/div/section/nav/div/ul/li[2]/a').click()
time.sleep(5)
driver.find_element(By.XPATH,'/html/body/div[2]/div/div/section/div/div/section/section/div/form/div/ul/li[{unit}]/a'.format(unit = str(unit))).click()
time.sleep(5)
driver.find_element(By.XPATH,'/html/body/div[2]/div/div/section/div/div/section/section/div/form/div/ul/li[{unit}]/ul/li[{section}]/div/span'.format(unit = str(unit), section = str(section))).click()
time.sleep(5)
driver.find_element(By.XPATH,'/html/body/div[2]/div/div/section/div/div/section/section/div/form/div/ul/li[{unit}]/ul/li[{section}]/ul/li[1]/div/div[1]/a'.format(unit = str(unit), section = str(section))).click()
time.sleep(10)

# Loop from this point
while True:
	try:
		# Rand time
		time.sleep(random.randint(start,stop))
		try:
			# Press Check
			driver.switch_to_frame(driver.find_element_by_id("content-iframe"))
			time.sleep(5)
			driver.switch_to_frame(driver.find_element_by_id("ScormContent"))
			time.sleep(5)
			driver.find_element(By.XPATH,'/html/body/section/footer/div[8]/nav/div[1]/div[1]/button').click()
			time.sleep(5)
			# Press Finish
			driver.find_element(By.XPATH,'/html/body/section/footer/div[5]/div[2]/div/div/div[2]/button').click()
		except:
			print('Essay mb?')
		# Press Next
		driver.switch_to.default_content()
		driver.find_element(By.XPATH,'/html/body/div[2]/div[2]/div[2]/div/a').click()
	except:
		print('Ive finished, not sure if it was successful')
		print('Error:\n', traceback.format_exc())
		break